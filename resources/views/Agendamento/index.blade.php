<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>FeeGow Teste </title>
    <!-- Bootstrap core CSS -->
    <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- Custom fonts for this template -->
    <link href="assets/vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
    <link href="assets/vendor/simple-line-icons/css/simple-line-icons.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
    <script src="https://code.jquery.com/jquery-migrate-3.1.0.js"></script>

    <link href="assets/plugins/jquery-confirm/css/jquery-confirm.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/landing-page.min.css" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-light bg-info static-top">
    <div class="container">
        <form>
            <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}"/>
            <div class="row">
                <div class="col-md-4">
                    <h3>Consulta de </h3>
                </div>
                <div class="col-md-4">
                    <select class="form-control" id="especialidade_id" name="especialidade"></select>
                </div>
                <div class="col-md-4">
                    <a href="javascript:void(0)" class="btn btn-success rounded-5" id="agendar_consultar">
                        AGENDAR
                    </a>
                </div>
            </div>
        </form>

    </div>
</nav>

{{-- Modal de Agendamento de Consulta--}}
<div id="form_agendamento"></div>

{{--  Profissionais --}}
<div id="professionals"></div>


<section class="testimonials text-center bg-light">
    <div class="container">
        <h2 class="mb-5">Agende sua Consulta !</h2>
        <div class="row">
            <div class="col-lg-12">
                <h5> Selecione uma de nossas especialidades , com os melhores profissionais do mercado !</h5>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<footer class="footer bg-light">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 h-100 text-center text-lg-left my-auto">
                <ul class="list-inline mb-2">
                    <li class="list-inline-item">
                        <a href="#">Sobre</a>
                    </li>
                    <li class="list-inline-item">&sdot;</li>
                    <li class="list-inline-item">
                        <a href="#">Contato</a>
                    </li>
                    <li class="list-inline-item">&sdot;</li>
                    <li class="list-inline-item">
                        <a href="#">Termos de Uso</a>
                    </li>
                    <li class="list-inline-item">&sdot;</li>
                    <li class="list-inline-item">
                        <a href="#">Políticas de Privacidade</a>
                    </li>
                </ul>
                <p class="text-muted small mb-4 mb-lg-0">&copy; FeeGow Test.</p>
            </div>
            <div class="col-lg-6 h-100 text-center text-lg-right my-auto">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item mr-3">
                        <a href="https://www.facebook.com/hugo.pessanha2" target="_blank">
                            <i class="fab fa-facebook fa-2x fa-fw"></i>
                        </a>
                    </li>

                </ul>
            </div>
        </div>
    </div>
</footer>

<!-- Bootstrap core JavaScript -->
<script src="assets/vendor/jquery/jquery.min.js"></script>
<script src="assets/vendor/js/jquery.mask.min.js"></script>
<script src="assets/plugins/jquery-confirm/js/jquery-confirm.min.js"></script>
<script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="assets/vendor/js/library.js"></script>


</body>

</html>
