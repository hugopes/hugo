<div class="modal fade" id="agendar_consulta_modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3> Preencha seus dados. </h3>
            </div>

            <form id="agendarConsultaForm" method="POST">

                <div class="modal-body">
                    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}"/>
                    <input type="hidden" value="{{@$profissional_id}}" name="profissional_id" id="profissional_id" />
                    <input type="hidden" value="{{@$especialidade_id}}" name="especialidade_id" id="especialidade_id" />

                    <div class="row">
                       <div class="col-md-12">

                           <div class="row">
                               <div class="col-6 col-sm-6">
                                   <div class="form-group">
                                       <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome completo" required>
                                   </div>
                               </div>

                               <div class="col-6 col-sm-6">
                                   <div class="form-group">
                                       <select class="form-control" name="source_id" id="source_id" required>
                                           <option value="">Como conheceu?</option>
                                             @foreach($patient_source->content as $source)
                                                 <option value="{{$source->origem_id}}">{{$source->nome_origem}}</option>
                                             @endforeach
                                       </select>
                                   </div>
                               </div>
                           </div>


                           <div class="row">
                               <div class="col-6 col-sm-6">
                                   <div class="form-group">
                                       <input type="date" name="nascimento" id="nascimento" class="form-control" placeholder="Nascimento" required>
                                   </div>
                               </div>

                               <div class="col-6 col-sm-6">
                                   <div class="form-group">
                                       <input type="text" name="cpf" id="cpf" class="form-control" placeholder="CPF" required>
                                   </div>
                               </div>
                           </div>

                       </div>
                    </div>
                </div>

                <div class="modal-footer">

                    <button type="submit" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <input class="btn btn-success rounded" type="submit" value="SOLICITAR HORÁRIOS" />

                </div>

            </form>

        </div>
    </div>
</div>

<script src="assets/vendor/js/library.js"></script>

