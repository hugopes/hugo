<section class="testimonials text-center bg-white">
    <div class="container">
        <h2 class="mb-5">Profissionais.</h2>
        <div class="row">
            @foreach($profissionais->content as $prof)
                <div class="col-md-4">
                    <div class="professionals">
                        <div class="features-icons-item mx-auto mb-5 mb-lg-0 mb-lg-3">

                            @if(@$prof->foto != null)
                                <img class="img-fluid rounded-circle mb-3" src="{{$prof->foto}}" alt="{{$prof->nome}}" title="{{$prof->nome}}" style="width: 120px;">
                            @else
                                <img class="img-fluid rounded-circle mb-3" src="assets/img/sem-foto.png" alt="sem foto" title="sem foto" style="width: 120px;">
                            @endif

                            <h4>{{@$prof->nome}}</h4>

                            @if($prof->conselho)
                                <br />
                                <small>{{$prof->conselho}}:{{$prof->documento_conselho}}</small>
                            @endif

                            <p class="font-weight-light mb-0">
                                @if(@$prof->especialidades)
                                    @foreach(@$prof->especialidades as $especialidade)
                                       <b>{{ @$especialidade->nome_especialidade }}</b> <br />
                                    @endforeach
                                @endif
                            </p>

                            <hr />

                            <button class="btn btn-success rounded-0" onclick="agendarConsulta({{$prof->profissional_id}},{{$especialidade_id}})"> AGENDAR</button>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
</section>
