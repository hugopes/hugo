//Variável Global token_api;
token_api = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJmZWVnb3ciLCJhdWQiOiJwdWJsaWNhcGkiLCJpYXQiOiIxNy0wOC0yMDE4IiwibGljZW5zZUlEIjoiMTA1In0.UnUQPWYchqzASfDpVUVyQY0BBW50tSQQfVilVuvFG38";
base_url  = "https://api.feegow.com.br/api";


$('#cpf').mask('999.999.999-99');

/**
 * @author Hugo Tavares Pessanha
 * @info Função para pegar as especialidades da API
 *       demonstrando o uso pelo AJAX
 */
$(function() {
    $.ajax({
        type: 'GET',
        url: base_url + '/specialties/list?x-access-token='+token_api,
        dataType: 'JSON',
        success: function (data) {

            var items = ["<option value=''>Selecione a especialidade</option>"];
            $.each(data.content, function (key, valor) {
                items.push("<option value='" + valor['especialidade_id'] + "'>" + valor['nome'] + "</option>");
            });

            $("#especialidade_id").append(items);
        }
    });
});


/**
 * @author Hugo Tavares Pessanha
 * @info Puxa os Profissionais , diferente da de cima
 *       pega pela API do Laravel.
 */
$("#agendar_consultar").click(function(){

    var especialidade_id = $("#especialidade_id").val();
    var token = $("#_token").val();

    if(especialidade_id == ""){
        alert('Selecione uma especialidade');
        return false;
    }

    $.ajax({
        type: 'POST',
        url: '/lista-profissionais',
        dataType: 'JSON',
        data:{'_token':token,'token_api':token_api,'especialidade_id':especialidade_id},
        success: function (data) {
            $("#professionals").html('');
            $("#professionals").html(data);
            $("#professionals").fadeIn();

        }
    })
});

/**
 * @author Hugo Tavares
 * @info Função para chamar um Modal do formulario de agendamento.
 * @param profissional_id
 * @param especialidade_id
 */

function agendarConsulta(profissional_id,especialidade_id){

    var token = $("#_token").val();

    $.ajax({
        type: 'POST',
        url: '/modal-agendar-consulta',
        dataType: 'JSON',
        data:{'_token':token,'token_api':token_api,'profissional_id':profissional_id,'especialidade_id':especialidade_id},
        success: function (data) {

            $("#form_agendamento").html(data);
            $("#agendar_consulta_modal").modal();

        }
    })
}

/**
 * @author Hugo Tavares Pessanha.
 * @info Formulario em AJAX do agendamento da consulta , com uma mensagem de retorno
 */

$("#agendarConsultaForm").bind("submit", function (a) {

    a.preventDefault();

    var dados = $(this).serialize();

    $.ajax({
        url: "/agendar-consulta",
        type: "POST",
        dataType: "JSON",
        data: dados,
        success: function (data) {

            if (data.mensagem == 'erro') {
                $.alert({
                    icon: 'fa fa-frown-o',
                    type: 'red',
                    title: 'Whoops!',
                    content: 'Ocorreu algum erro!',
                });
                return false;
            }
            if (data.mensagem == 'gravado') {
                $.alert({
                    icon: 'fa fa-check-circle-o',
                    type: 'green',
                    title: 'Perfeito!',
                    content: 'Consulta Agendada com Sucesso !',
                });

               // $("#agendar_consulta_modal").hide();
                $("#agendar_consulta_modal").modal('hide');
            }

        }
    })
})


