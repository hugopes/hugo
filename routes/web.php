<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/

Route::get('/','Agendamento\Agendamentos@index');

Route::post('/lista-profissionais','Agendamento\Agendamentos@listaProfissionais');
Route::post('/modal-agendar-consulta','Agendamento\Agendamentos@modalAgendarConsulta');
Route::post('/agendar-consulta','Agendamento\Agendamentos@agendarConsulta');
