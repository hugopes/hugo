<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agendamentos extends Model
{

    protected $table = 'agendamentos';
    protected $primaryKey = 'agendamento_id';

    protected $fillable = [ 'speciality_id',
                            'professional_id',
                            'name',
                            'cpf',
                            'source_id',
                            'birthdate',
                            'date_time' ];

    public $timestamps = false;

}
