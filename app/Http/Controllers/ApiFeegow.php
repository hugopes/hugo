<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use GuzzleHttp\Client as Curl;

class ApiFeegow extends Controller
{
    use AuthorizesRequests,DispatchesJobs,ValidatesRequests;


    private $server_api;
    private $token_api;
    private $ativa;
    private $curl;


    public function __construct(Curl $curl)
    {
        $this->curl = $curl;
        $this->server_api = "https://api.feegow.com.br/api";
    }

    /**
     * @author Hugo Tavares Pessanha
     */
    public function pegaProfissionaisApi($uri,$access_token,$parameters){

        $response = $this->curl->request('GET', $this->server_api.$uri.$access_token.'&especialidade_id='.$parameters.'&ativo=1', [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);

        return json_decode($response->getBody());
    }

    /**
     * @author Hugo Tavares Pessanha.
     */
    public function pegaPacientSourceApi($uri,$access_token){

        $response = $this->curl->request('GET', $this->server_api.$uri.$access_token, [
            'http_errors' => false,
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);

        return json_decode($response->getBody());
    }

}
