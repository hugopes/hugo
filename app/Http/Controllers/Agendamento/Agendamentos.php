<?php

namespace App\Http\Controllers\Agendamento;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\ApiFeegow;
use App\Models\Agendamentos as AgendamentoDB;


class Agendamentos extends Controller
{

    private $api_token;
    private $host;
    private $api_feegow;

    public function __construct(ApiFeegow $feegow_api)
    {
        $this->api_token = "?x-access-token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJmZWVnb3ciLCJhdWQiOiJwdWJsaWNhcGkiLCJpYXQiOiIxNy0wOC0yMDE4IiwibGljZW5zZUlEIjoiMTA1In0.UnUQPWYchqzASfDpVUVyQY0BBW50tSQQfVilVuvFG38";
        $this->api_feegow = $feegow_api;
    }

    /**
     * @author Hugo Tavares Pessanha da Silva.
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(){
        return view('Agendamento.index');
    }

    /**
     * @author Hugo Tavares Pessanha da Silva.
     * @info Lista profissionais
     */
    public function listaProfissionais(Request $request)
    {
        /** @var  $especialidade_id */
        $especialidade_id = $request->get('especialidade_id');
        $profissionais    = $this->api_feegow->pegaProfissionaisApi('/professional/list',$this->api_token,$especialidade_id);

        $view = (string) view('Agendamento.profissionais',compact('profissionais','especialidade_id'));
        return json_encode($view);

    }

    /**
     * @author Hugo Tavares Pessanha da Silva.
     * @return false|string
     */
    public function modalAgendarConsulta(Request $request){

        /** Request */
            $profissional_id  = $request->get('profissional_id');
            $especialidade_id = $request->get('especialidade_id');

        /** @var  $patient_source */
        $patient_source    = $this->api_feegow->pegaPacientSourceApi('/patient/list-sources',$this->api_token);

        $modal = (string)view('Agendamento.Modals.modal-agendar-consulta',compact('patient_source','profissional_id','especialidade_id'));
        return json_encode($modal);

    }

    /**
     * @author Hugo Tavares Pessanha
     * @info Agenda consulta
     */
    public function agendarConsulta(Request $request){

        /** @var  $dados_request */
        $dados_request  = [
            'speciality_id'    => $request->get('especialidade_id'),
            'professional_id'  => $request->get('profissional_id'),
            'name'             => $request->get('nome'),
            'cpf'              => $request->get('cpf'),
            'source_id'        => $request->get('source_id'),
            'birthdate'        => $request->get('nascimento'),
            'date_time'        => date('Y-m-d H:i:s')
        ];

        /** @var  $agendamento */
        $agendamento = AgendamentoDB::create($dados_request);


        if($agendamento){
            return json_encode(['mensagem' => 'gravado']);
        }else{
            return json_encode(['mensagem' => 'erro']);
        }

    }

}
